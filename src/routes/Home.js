import {React, useEffect} from "react";
import {Card, Button} from 'react-bootstrap';
import "../css/Home.css";
import AOS from 'aos';
import 'aos/dist/aos.css';
import { ProjectCardSummaries } from "../constants/projectCardSummaries";

import { imageAssets } from "../constants/imageAssets";
import { about } from "../constants/about";

const Home = () => {
    useEffect(() => {
        document.title = "About"
     }, []);

    AOS.init();

    return(
        <div className="container">
            <div className="firstPageHome">
                <div className="topArea">
                    <div data-aos="fade-right"
                    data-aos-duration="1500"
                    className="imageArea">
                        <img src={imageAssets["domas"]} className="DomasImage" alt="Domas Profile"/>
                    </div>
                    <div data-aos="fade-left"
                    data-aos-duration="1500"
                    className="textArea">
                        <h2 className="subTitle">A Bit About Me</h2>
                        <div className="topParagraph">{about.about_me}</div>
                    </div>
                </div>

                <div data-aos="fade-up"
                data-aos-easing="linear"
                data-aos-duration="1500" 
                className="portfolioLinksArea">
                    <div className="portfolioLinks">
                        <a target="_blank" href="https://gitlab.com/users/augustaitis.domas/projects" rel="noreferrer">
                            <img src={imageAssets["gitlab"]} className="portfolioLinkImage" alt="GitLab Logo"/>
                        </a>
                        <a target="_blank" href="https://github.com/DomasAugustaitis" rel="noreferrer">
                            <img src={imageAssets["github"]} className="portfolioLinkImage" alt="GitHub Logo"/>
                        </a>
                        <a target="_blank" href="https://www.linkedin.com/in/domas-augustaitis-b591b6151/" rel="noreferrer">
                            <img src={imageAssets["linkedin"]} className="portfolioLinkImage" alt="LinkedIn Logo"/>
                        </a>
                    </div>
                </div>

                <div className="scrollAnimationArea">
                    <div className="arrow">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>

            <div className="projectsArea" id="projects">
                <h1>Projects</h1>
            </div>

            {ProjectCardSummaries.map((project, i) => {
                let imageFade = i%2===0 ? "fade-right" : "fade-left";
                let cardFade = i%2!==0 ? "fade-right" : "fade-left";
                let cardDirection = i%2===0 ? "ltr" : "rtl";

                return(
                    <div key={i} className="projectRow" style={{direction: cardDirection}}>
                        <div className="cardImageArea" data-aos={imageFade}>
                                <img src={project.image} className="cardImage" alt="project"/>
                        </div>
                        <Card data-aos={cardFade} className="projectCard">
                            <Card.Body className="cardBodyArea">
                                <Card.Title>{project.title}</Card.Title>
                                <Card.Text>{project.summary}</Card.Text>
                                <Button variant="outline-primary" href={project.page}>Explore</Button>
                            </Card.Body>
                        </Card>
                    </div>
                )
            })}
        </div>
    )
}

export default Home;