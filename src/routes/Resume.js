import {React, useEffect} from "react";
import "../css/Resume.css";
import { Button } from "react-bootstrap";
import { Timeline, TimelineItem} from 'vertical-timeline-component-for-react';

export default function Resume(props) {

  useEffect(() => {
    document.title = "Resume"
  }, []);

  return (
    <div className="resumeContainer">
       

        <div className="downloadButton">
            <Button
            variant="outline-primary"
            size="lg"
            href="https://drive.google.com/file/d/16Y1ju9j1LtybmWdhZCQqst1u1kYBwnLJ/view?usp=sharing">
                Request Full CV
            </Button>
        </div>
        
        <h1>Employment History</h1>

        <Timeline lineColor={'#ddd'}>
          <TimelineItem
            key="IPL_V"
            dateText="08/2022 – Present"
            dateInnerStyle={{ background: '#76bb7f' }}
            bodyContainerStyle={{
              background: '#ddd',
              padding: '20px',
              borderRadius: '8px',
              boxShadow: '0.5rem 0.5rem 2rem 0 rgba(0, 0, 0, 0.2)',
            }}
          >
            <h3>Innovation Platform Lead</h3>
            <h4>New Technologies & innovations</h4>
            <p/>
            <div>
            <p>Created new backend microservices for an internal innovations platform using the best company and industry standards:
              <li className="resumeBulletList">Used Java 19 SpringBoot framework to create new controllers, services, and entity managers.</li>
              <li className="resumeBulletList">Created Dockerfiles to containerise these microservices and pushed them to GCP Google Kubernetes Engine (GKE).</li>
              <li className="resumeBulletList">Created automated Azure DevOps pipelines that build and deploy our code to GKE once it is pushed to Git and passes all tests.</li>
            </p>
            <p>
              Managed a team of platform developer contractors:
              <li className="resumeBulletList">Implemented Scrum agile workflow – led daily stand-up, biweekly sprint planning, Jira backlog grooming and demo meetings.</li>
            </p>
            <p>
            Implemented BDD framework within the platform backend using Gherkin and Cucumber.
            </p>
            <p>
              Promoted open-source innovation culture within the company:
              <li className="resumeBulletList">Created open-source git repository to share knowledge and reuse code within different innovation projects.</li>
              <li className="resumeBulletList">Supported youth innovation projects and provided my technical expertise.</li>
            </p>
            </div>
          </TimelineItem>
          <TimelineItem
            key="001"
            dateText="09/2020 - 08/2022"
            dateInnerStyle={{ background: '#e86971' }}
            bodyContainerStyle={{
              background: '#ddd',
              padding: '20px',
              borderRadius: '8px',
              boxShadow: '0.5rem 0.5rem 2rem 0 rgba(0, 0, 0, 0.2)',
            }}
          >
            <h3>Software Engineer Graduate</h3>
            <p/>
            <p>
              Participated in the 2-year graduate scheme as a Software Engineer. Throughout the scheme I had 3 different team rotations that are described in detail below:
            </p>
          </TimelineItem>
          <TimelineItem
            key="002a"
            style={{ color: '#e86971' }}
            dateComponent={(
              <div style={styles.redSubTimeline}>
                09/2021 - 08/2022
              </div>
            )}
            bodyContainerStyle={{
              background: '#ddd',
              padding: '20px',
              borderRadius: '8px',
              boxShadow: '0.5rem 0.5rem 2rem 0 rgba(0, 0, 0, 0.2)',
            }}
          >
            <h4>Software Engineer Graduate</h4>
            <h5>Consumer IoT</h5>

            <p/>
            <div>
              <p>
                As part of the agile squad, I worked on creating complex Java (Springboot) scripts to improve the
                quality of existing services within the back-end of consumer IoT products:
                <li className="resumeBulletList">Created new and improved existing functions.</li>
                <li className="resumeBulletList">Created new automated Maven tests and improved existing code coverage</li>
              </p>
              <p>
              Took a role of the scrum master in an internal TechBoost project and ensured team's efficiency through
              the Agile methodology:
                <li className="resumeBulletList">Led stand-up, sprint planning, backlog grooming, sprint demo and retrospective meetings for a team of 8 developers.</li>
                <li className="resumeBulletList">Aligned future roadmap with product owners and stakeholders and made sure that our MVP goals are appropriately reflected in each sprint.</li>
                <li className="resumeBulletList">In addition to scrum mastering, I have also contributed to the project by creating UI components with ReactJS, creating back-end functionality with NodeJS and MongoDB and designing web pages with Figma.</li>
              </p>
              <p>
              Organised and led the biggest internal employee Virtual-Reality / Augmented-Reality Hackathon, which resulted in 5 new proof-of-concept solutions to the hybrid working model:
                <li className="resumeBulletList">Communicated with senior managers within the company to ensure the hackathon had senior sponsor support.</li>
                <li className="resumeBulletList">Organised and led multiple workshop and support sessions.</li>
                <li className="resumeBulletList">Collaborated with winning teams to create future internal development opportunities and promoted VR/AR technology within the company.</li>
              </p>
              <p>
                As part of the Business Coding Opportunities, I led a team of 4 developers working on creating an automated data management tool (using Python).
              </p>
            </div>
          </TimelineItem>
          <TimelineItem
            key="002b"
            style={{ color: '#e86971' }}
            dateComponent={(
              <div style={styles.redSubTimeline}>
                03/2021 - 08/2021
              </div>
            )}
            bodyContainerStyle={{
              background: '#ddd',
              padding: '20px',
              borderRadius: '8px',
              boxShadow: '0.5rem 0.5rem 2rem 0 rgba(0, 0, 0, 0.2)',
            }}
          >
            <h4>Software Engineer Graduate</h4>
            <h5>IoT Platforms</h5>
            <p/>
            <div>
              <p>
                Created a Java-based load-test tool for Device Management platform. It uses Eclipse Leshan to create many Lightweight M2M devices and upload them to the platform to test its capacity management.
              </p>
              <p>
                One of the founding members of the company-internal youth coding club:
                <li className="resumeBulletList">Led a team of 4 developers creating Python upskilling sessions for all employees.</li>
              </p>
            </div>
          </TimelineItem>
          <TimelineItem
            key="002c"
            style={{ color: '#e86971' }}
            dateComponent={(
              <div style={styles.redSubTimeline}>
                09/2020 - 02/2021
              </div>
              
            )}
            bodyContainerStyle={{
              background: '#ddd',
              padding: '20px',
              borderRadius: '8px',
              boxShadow: '0.5rem 0.5rem 2rem 0 rgba(0, 0, 0, 0.2)',
            }}
          >
            <h4>Software Engineer Graduate</h4>
            <h5>New Technologies & Innovation</h5>
            <p />
            <div>
            <p>
              Researched possible VR/AR networking modules for Unity. Used Normcore.io and Unity (C#) to develop a VR/AR V-Lab platform used for immersive collaboration.
            </p>
            <p>
              Developed an app that was used as a gamified learning platform by refugees in Turkey:
              <li className="resumeBulletList">I used JavaScript and React Native to create app screens and their functionalities.</li>
              <li className="resumeBulletList">Implemented the game within our app using Unity and React Native Unity View module.</li>
            </p>
            </div>
          </TimelineItem>
          <TimelineItem
            key="002d"
            dateText="04/2009 - 11/2010"
            dateInnerStyle={{ background: '#61b8ff'}}
            bodyContainerStyle={{
              background: '#ddd',
              padding: '20px',
              borderRadius: '8px',
              boxShadow: '0.5rem 0.5rem 2rem 0 rgba(0, 0, 0, 0.2)',
            }}
          >
            <h3>Data Analyst</h3>
            
            <p>
              Took part in the Delivery Cost Optimisation project. We partnered with Vilnius University to create an AI-driven logistics route planner. I was tasked with:
              <li className="resumeBulletList">
                Gathering rich data for company-internal projects from a relational DB using Microsoft SQL Server Management Studio and complex T-SQL queries.
              </li>
              <li className="resumeBulletList">
                Validating, visualising, and analysing extracted data with Python and Microsoft Excel Tools.
              </li>
            </p>
          </TimelineItem>
        </Timeline>
    </div>
  );
}

const styles = {
  redSubTimeline: {
    position: "absolute",
    right: "10px",
    top: "-10px",
    padding: '10px',
    background: '#f3f3f3',
    color: 'black',
    width: "170px",
    height: "50px",
    borderRadius: "10px",
    textAlign: "center",
  }
}