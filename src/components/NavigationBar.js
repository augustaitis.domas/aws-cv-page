import React from "react";
import '../css/NavigationBar.css';
import { HashLink } from 'react-router-hash-link';

export function NavigationBar() {    
    return (
        <div className="navBar">
            <div className="title">
                <a className="name" href="/">Domas Augustaitis</a>
                <div className="role">Full-Stack Developer</div>
            </div>
            <div className="navButtons">
                <a className="button" href="/">About</a>
                <div>|</div>
                <a className="button" href="/resume">Resume</a>
                <div>|</div>
                <HashLink to="/#projects" className="button">Projects</HashLink>
                <div>|</div>
                <a className="button" href="/contact">Contact</a>
            </div>
        </div>
    )
}