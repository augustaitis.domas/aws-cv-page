import React from "react";
import "../css/FooterBar.css";

export function FooterBar() {
    return (
        <div className="FooterContainer">
            <div className="FooterText">
                <div>If you would like to reach out to me to talk about my past experience, past projects or future prospects, please contact me using the following email.</div>
            </div>
            <div className="FooterContact">
                <div className="emailDiv">
                    <div><b>Email:</b> domasaugustaitiscv@gmail.com</div>
                </div>
                <div className="locationDiv">
                    <div><b>Location:</b> England, UK</div>
                </div>
            </div>
        </div>
    )
}
