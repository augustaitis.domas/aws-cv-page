export const ProjectCardSummaries = [
    {
        id: 1,
        title: "Portfolio Website",
        image: require("../images/portfolioWebsite.PNG"),
        summary: "A responsive React.js and AWS Amplify web platform that contains the details of my resume and information on my past projects and their examples.",
        page: "portfolioWebpage"
    },
    {
        id: 2,
        title: "Portfolio Webpage",
        image: require("../images/redux.png"),
        summary: "A React.js protfolio webpage that contains my resume, past projects and contacts.",
    }
]