export const imageAssets = {
    domas: require("../images/Domas_Augustaitis.jpg"),
    gitlab: require("../images/gitlab.png"),
    github: require("../images/github.png"),
    linkedin: require("../images/linkedin.png"),
    redux: require("../images/redux.png"),
    portfolioWebsite: require("../images/portfolioWebsite.PNG"),
}