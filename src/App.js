import './App.css';
import { Route, Routes, BrowserRouter, useLocation } from "react-router-dom";
import React, { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import ReactGA from 'react-ga4'

import { NavigationBar } from './components/NavigationBar';
import { FooterBar } from './components/FooterBar';
import Home from './routes/Home';
import Resume from './routes/Resume';
import Contact from './routes/Contact';

function App() {

  ReactGA.initialize('G-YEZWGCXBQD');

  const ReactGATracker = () => {
    let loc = useLocation();
    useEffect(() => {
      ReactGA.send('pageview');
    }, [loc])

    return (
      <div className='ReactGATrackerDiv'></div>
    )
  }

  const NavigatorDiv = () => {
    return (
    <div className="App">
        <nav>
          <NavigationBar />
        </nav>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/resume" element={<Resume />} />
          <Route path="/contact" element={<Contact />} />
        </Routes>
        <footer>
          <FooterBar />
        </footer>
      </div>
    )
  }

  return (
    <BrowserRouter>
    <ScrollToTop />
      <NavigatorDiv />
      <ReactGATracker />
    </BrowserRouter>
  );
}

const ScrollToTop = () => {
  const { pathname, hash } = useLocation();

  useEffect(() => {
    
    if (hash === "") window.scrollTo(0, 0);
  }, [pathname, hash]);

  return null;
}


export default App;
